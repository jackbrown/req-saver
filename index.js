#!/usr/bin/env node
var fetch = require("node-fetch");
var crypto = require("crypto");
var fs = require("fs");
var path = require("path");

exports.reExecute = reExecute;
exports.start = start;

function reExecute(request) {
    var options = {
        method: request.method,
        headers: request.headers,
    };
    if (request.method.toLowerCase() != "get") {
        if (typeof request.body == "object") {
            request.body = JSON.stringify(request.body);
        }
        options.body = request.body;
    }
    let qs =  queryString(request.query);
    var fullUrl = request.url;
    if (qs){
        fullUrl+= "?" + qs;
    }
    return fetch(fullUrl, options).then(function (data) {
        return data;
    })
}

function queryString(search) {
    /**
     * taked from https://stackoverflow.com/questions/14525178/is-there-any-native-function-to-convert-json-to-url-parameters
     *  */
    var isObj = function (a) {
        if ((!!a) && (a.constructor === Object)) {
            return true;
        }
        return false;
    };
    var _st = function (z, g) {
        return "" + (g != "" ? "[" : "") + z + (g != "" ? "]" : "");
    };
    var fromObject = function (params, skipobjects, prefix) {
        if (skipobjects === void 0) {
            skipobjects = false;
        }
        if (prefix === void 0) {
            prefix = "";
        }
        var result = "";
        if (typeof (params) != "object") {
            return prefix + "=" + encodeURIComponent(params) + "&";
        }
        for (var param in params) {
            var c = "" + prefix + _st(param, prefix);
            if (isObj(params[param]) && !skipobjects) {
                result += fromObject(params[param], false, "" + c);
            } else if (Array.isArray(params[param]) && !skipobjects) {
                params[param].forEach(function (item, ind) {
                    result += fromObject(item, false, c + "[" + ind + "]");
                });
            } else {
                result += c + "=" + encodeURIComponent(params[param]) + "&";
            }
        }
        return result;
    };

    return fromObject(search)
}

function getRequestString(struct) {
    var template = `
let requestToFile=require("request-to-file");

return requestToFile.reExecute({
    url: "${struct.url}",
    method: "${struct.method}",
    headers: ${JSON.stringify(struct.headers, null, 4)},
    query: ${JSON.stringify(struct.query, null, 4)},
    body: ${JSON.stringify(struct.body, null, 4)},
}).then(function(data){
    console.log("Status: ", data.status)
    return data.text().then(function (dataText) {
        try {
            console.log(JSON.stringify(JSON.parse(dataText), null, 2));
        } catch (e) {
            console.log(dataText)
        } 
    }) 
})`;
    return template;
}

/**
 * 
 * @param {Object} options 
 * @param {String} options.newUrlBase  
 * @param {Function} options.shouldBeSaved: false|string_path_folder_destination
 * @param {Function} options.customSave: void
 * @returns 
 */
function start({newUrlBase,shouldBeSaved,customSave}) {

    if (customSave!=undefined && typeof customSave!='function'){
        throw new Error("The `customSave` argument should be a function");
    }
    else if (customSave==undefined){
        customSave=function(){}
    }
    if (shouldBeSaved!=undefined && typeof shouldBeSaved!='function'){
        throw new Error("The `shouldBeSaved` argument should be a function");
    }
    else if (customSave==undefined){
        customSave=function(){ return false; }
    }


    console.log("<<<<< Inicializando el request-to-file >>>>>")
    return function (req, res, next) {
        if (newUrlBase==undefined || newUrlBase==''){
            newUrlBase=req.protocol + '://' + req.get('host');
        }
        var struct = {
            headers: {},
            method: req.method,
            path: req.path,
            query: {},
            body: {},
            url: newUrlBase+ req.originalUrl.split("?")[0],
            ip: req.ip,
            protocol: req.protocol,
            hostname: req.hostname,
            host: req.headers.host
        }
        if (req.headers) {
            struct.headers = JSON.parse(JSON.stringify(req.headers));
            delete struct.headers["content-length"];
            delete struct.headers["if-none-match"];
        }
        if (req.query) {
            struct.query = JSON.parse(JSON.stringify(req.query))
        }
        if (req.body) {
            struct.body = JSON.parse(JSON.stringify(req.body))
        }
        res.on('finish', function onResFinish() {
    
            processOnFinish(struct, shouldBeSaved , customSave , req, res)
            
        });
        return next();
    }
}



function getUnique(struct) {
    var md5 = crypto.createHash('md5').update(struct).digest('hex');
    return md5;
}

function processOnFinish(struct, shouldBeSaved,  customSave, req, res) {
    struct = getRequestString(struct)
    var unique = getUnique(struct);
    customSave(struct,req,res);
    var encontrado = false;

    let pathToFolder=shouldBeSaved(req,res)
    if (fs.existsSync(pathToFolder)) {
        var list = fs.readdirSync(pathToFolder)
        for (var i = 0; i < list.length && !encontrado; i++) {
            if (list[i].indexOf(unique) != -1) {
                encontrado = true;
            }
        }
        if (!encontrado) {
            console.log("Saving request into file..")
            var url = req.url.split("/").join("-").split("?")[0];
            var a = new Date();
            fs.writeFileSync([pathToFolder, a.toISOString().split(":").join("-") + unique + '-' + res.statusCode + '-' + req.method + "-" + url + ".js"].join(path.sep), struct, 'utf8')
        } else {
            console.log("This request was previously saved:)")
        }
    }
}

